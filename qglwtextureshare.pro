QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += multimedia multimediawidgets

QT += opengl gui-private

DEFINES += UNICODE QT_THREAD_SUPPORT QT_CORE_LIB QT_GUI_LIB

#INCLUDEPATH += /usr/include/gstreamer-1.0 \
#  /usr/lib/x86_64-linux-gnu/gstreamer-1.0/include \
#  /usr/include/glib-2.0 \
#  /usr/lib/x86_64-linux-gnu/glib-2.0/include \
#  /usr/include/orc-0.4
#QT += x11extras

DEFINES += IS_PI
INCLUDEPATH += /home/brucegoose/raspi/sysroot/usr/include/libdrm \
  /home/brucegoose/raspi/sysroot/usr/include/gstreamer-1.0 \
  /home/brucegoose/raspi/sysroot/usr/local/include/gstreamer-1.0 \
  /home/brucegoose/raspi/sysroot/usr/local/lib/gstreamer-1.0/include \
  /home/brucegoose/raspi/sysroot/usr/include/gstreamer-1.0 \
  /home/brucegoose/raspi/sysroot/usr/include/glib-2.0 \
  /home/brucegoose/raspi/sysroot/usr/include/orc-0.4 \
  /home/brucegoose/raspi/sysroot/usr/lib/arm-linux-gnueabihf/glib-2.0/include
LIBS += -lEGL \
  -lGLESv2


LIBS += -lgstreamer-1.0 \
  -lgobject-2.0 \
  -lglib-2.0 \
  -lgstvideo-1.0 \
  -lgstbase-1.0 \
  -lgstgl-1.0 \
  -lGLU \
  -lGL

CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

# Header files
HEADERS += gstthread.h \
    pipeline.h \
    qglrenderer.h \
    AsyncQueue.h \

# Source files
SOURCES +=  main.cpp \
    gstthread.cpp \
    pipeline.cpp \
    qglrenderer.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
